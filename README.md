[![Try it !](https://img.shields.io/static/v1?label=Try%20it!&message=florent_tassy.gitlab.io/planning/&color=informational&style=flat-square)](https://florent_tassy.gitlab.io/planning/)

# Planning generator

This web app is intended to generate weekly plannings.

The use case for which it was written is the following :
- We have an amount of hours to dispatch in weekly planning
- We know the weekdays we will use these hours
- We know some days will be out of this weekly planning (holidays...) 

The web app dispatches the days according to these constraints. It outputs the generated planning in several forms :  
- An hyperlink
- A list of days
- A calendar
- A table
- Several files
