import type Period from "./Period"

export type PlanningParams = {
    title: string,
    duration: number,
    isMondayTaught: boolean,
    isTuesdayTaught: boolean,        
    isWednesdayTaught: boolean,
    isThursdayTaught: boolean,
    isFridayTaught: boolean,
    teachingStartDate: Date,
    hoursPerDay: number,
    trimExtraHoursBy: "start" | "end",
    expectedAssessment: number,
    noTeachingPeriods: Period[],
    showHe: boolean
}
