import Day from "./Day";
import type Period from "./Period";
import type { PlanningParams } from "./types";

export default class Planning {
    title: string;
    duration: number;
    isMondayTaught: boolean;
    isTuesdayTaught: boolean;
    isWednesdayTaught: boolean;
    isThursdayTaught: boolean;
    isFridayTaught: boolean;
    teachingStartDate: Date;
    teachingDays: number[] = [];
    trimExtraHoursBy: "start" | "end";
    noTeachingPeriods: Period[] = [];
    hoursPerDay: number;
    expectedAssessment: number;
    showHe: boolean

    constructor(planningParams: PlanningParams) {
        this.title = planningParams.title;
        this.duration = planningParams.duration;
        this.isMondayTaught = planningParams.isMondayTaught;
        this.isTuesdayTaught = planningParams.isTuesdayTaught;
        this.isWednesdayTaught = planningParams.isWednesdayTaught;
        this.isThursdayTaught = planningParams.isThursdayTaught;
        this.isFridayTaught = planningParams.isFridayTaught;
        this.teachingStartDate = planningParams.teachingStartDate;
        this.trimExtraHoursBy = planningParams.trimExtraHoursBy;
        this.expectedAssessment = planningParams.expectedAssessment;
        this.noTeachingPeriods = planningParams.noTeachingPeriods || [];
        this.hoursPerDay = planningParams.hoursPerDay;
        this.showHe = planningParams.showHe;

        if (this.isMondayTaught) {
            this.teachingDays.push(1);
        }

        if (this.isTuesdayTaught) {
            this.teachingDays.push(2);
        }

        if (this.isWednesdayTaught) {
            this.teachingDays.push(3);
        }

        if (this.isThursdayTaught) {
            this.teachingDays.push(4);
        }

        if (this.isFridayTaught) {
            this.teachingDays.push(5);
        }
    }

    get dayList(): Day[] {
        let taughtHours: number = 0;
        let taughtDays: number = 0;
        let dayList: Day[] = [];
        let dateCursor: Date = this.teachingStartDate;
        const shorterDayDuration: number = this.duration % this.hoursPerDay;

        while (taughtHours < this.duration) {
            // Est-ce un jour d'enseignement ?
            if (this.teachingDays.includes(dateCursor.getDay())) {

                let isTeachingDay: boolean = true;

                // Tombe un jour sans cours ?
                for (let period of this.noTeachingPeriods) {
                    // console.debug("Test if", dateCursor, "is in", period);
                    if (period.isDateIncluded(dateCursor)) {
                        // console.debug("Jour férié !!", dateCursor);
                        dayList.push(new Day(`⚠️ Pas de cours - ${period.description}|-|-`, new Date(dateCursor)));
                        isTeachingDay = false;
                        break;
                    }
                }

                // Tombe un jour travaillé
                if (isTeachingDay) {
                    taughtDays += 1;

                    // Tronque les heures en trop
                    if (taughtDays === 1 && this.trimExtraHoursBy === "start" && shorterDayDuration !== 0) {
                        taughtHours += shorterDayDuration;
                    } else if (shorterDayDuration !== 0 && (taughtHours + this.hoursPerDay) > this.duration && this.trimExtraHoursBy === "end") {
                        taughtHours += shorterDayDuration;
                    } else {
                        taughtHours += this.hoursPerDay;
                    }

                    // Vérifie si c'est un jour d'évaluation
                    if ((taughtHours >= this.expectedAssessment) && ((taughtHours - this.hoursPerDay) < this.expectedAssessment)) {
                        // C'est la date de l'évaluation intermédiaire
                        dayList.push(new Day(`📝 Cours et évaluation intermédiaire|${taughtDays}|${taughtHours}`, new Date(dateCursor)));
                    } else if (taughtHours >= this.duration) {
                        // C'est la date de l'évaluation finale
                        dayList.push(new Day(`📝 Cours et évaluation finale|${taughtDays}|${taughtHours}`, new Date(dateCursor)));
                    } else {
                        // Cours normal
                        dayList.push(new Day(`Cours|${taughtDays}|${taughtHours}`, new Date(dateCursor)));
                    }
                }
            }
            dateCursor.setDate(dateCursor.getDate() + 1);
        }

        return dayList;
    }

    pushNoTeachingPeriod(period: Period) {
        this.noTeachingPeriods.push(period);
    }

    pushNoTeachingPeriods(periods: Period[]) {
        periods.forEach(this.pushNoTeachingPeriod);
    }

    get calendarTitle() {
        return this.title;
    }

    isDateInDayList(day: Date, dayList: Day[]): Boolean {
        for (let dayOfList of dayList) {
            if (day.getFullYear() === dayOfList.dayDate.getFullYear() && day.getMonth() === dayOfList.dayDate.getMonth() && day.getDate() === dayOfList.dayDate.getDate()) {
                return true;
            }
        }
        return false;
    }

    getDayFromListByDate(date: Date, dayList: Day[]): Day {
        for (let dayOfList of dayList) {
            if (date.getFullYear() === dayOfList.dayDate.getFullYear() && date.getMonth() === dayOfList.dayDate.getMonth() && date.getDate() === dayOfList.dayDate.getDate()) {
                return new Day(dayOfList.dayName, dayOfList.dayDate);
            }
        }
        return new Day("", date);
    }
}
