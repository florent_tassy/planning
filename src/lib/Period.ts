import type Day from "./Day";

export default class Period {
    description: string;
    startDate: Date;
    endDate: Date;

    public constructor(description: string, startDate: Date, endDate?: Date) {
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate ? endDate : startDate;
    }

    public static getFromDay(day: Day): Period {
        return new Period(day.dayName, day.dayDate);
    }

    public isDateIncluded(date: Date): boolean {
        return  date >= this.startDate && date <= this.endDate;
    }

    public isDayIncluded(day: Day): boolean {
        return this.isDateIncluded(day.dayDate);
    }

    public isSingleDay(): boolean {
        return this.startDate.getFullYear() === this.endDate.getFullYear() &&
        this.startDate.getMonth() === this.endDate.getMonth() &&
        this.startDate.getDate() === this.endDate.getDate();
    }
}
