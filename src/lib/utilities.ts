import type { PlanningParams } from "./types";

export function jsDateToDdMmYyyy(date: Date): string {
    return date.getDate().toString().padStart(2, "0") + "/" + (date.getMonth() + 1).toString().padStart(2, "0") + "/" + date.getFullYear();
}

export function ddMmYyyyToJsDate(ddMmYyyy: string): Date | null {
    try {
        const parsedDdMmYyyy: string[] = ddMmYyyy.split("/");
        return new Date(Number.parseInt(parsedDdMmYyyy[2]), Number.parseInt(parsedDdMmYyyy[1]) - 1, Number.parseInt(parsedDdMmYyyy[0]), 12, 0, 0);
    } catch (err) {
        console.warn(`La date ${ddMmYyyy} semble invalide,\n${err}`);
        return null;
    }
}

export function toFrWeeklyDay(date: Date): string {
    let fullDay: string;

    switch (date.getDay()) {
        case 0:
            fullDay = "dimanche";
            break;
        case 1:
            fullDay = "lundi";
            break;
        case 2:
            fullDay = "mardi";
            break;
        case 3:
            fullDay = "mercredi";
            break;
        case 4:
            fullDay = "jeudi";
            break;
        case 5:
            fullDay = "vendredi";
            break;
        case 6:
            fullDay = "samedi";
            break;
        default:
            fullDay = "Erreur dans toFrWeeklyDay";
    }

    return fullDay;
}

export function getFrMonth(date: Date) {
    let fullMonth: string;

    switch (date.getMonth()) {
        case 0:
            fullMonth = "janvier";
            break;
        case 1:
            fullMonth = "février";
            break;
        case 2:
            fullMonth = "mars";
            break;
        case 3:
            fullMonth = "avril";
            break;
        case 4:
            fullMonth = "mai";
            break;
        case 5:
            fullMonth = "juin";
            break;
        case 6:
            fullMonth = "juillet";
            break;
        case 7:
            fullMonth = "août";
            break;
        case 8:
            fullMonth = "septembre";
            break;
        case 9:
            fullMonth = "octobre";
            break;
        case 10:
            fullMonth = "novembre";
            break;
        case 11:
            fullMonth = "décembre";
            break;
        default:
            fullMonth = "Erreur dans getFrMonthYyyy"
    }

    return fullMonth;
}

export function jsonToUrlParams(planningParams: PlanningParams): string {
    let urlPlanningParams: URLSearchParams = new URLSearchParams();

    urlPlanningParams.set("title", planningParams.title);
    urlPlanningParams.set("duration", planningParams.duration.toString());
    urlPlanningParams.set("expectedAssessment", planningParams.expectedAssessment.toString());
    urlPlanningParams.set("isMondayTaught", planningParams.isMondayTaught ? "true" : "false");
    urlPlanningParams.set("isTuesdayTaught", planningParams.isTuesdayTaught ? "true" : "false");
    urlPlanningParams.set("isWednesdayTaught", planningParams.isWednesdayTaught ? "true" : "false");
    urlPlanningParams.set("isThursdayTaught", planningParams.isThursdayTaught ? "true" : "false");
    urlPlanningParams.set("isFridayTaught", planningParams.isFridayTaught ? "true" : "false");
    urlPlanningParams.set("hoursPerDay", planningParams.hoursPerDay.toString());
    urlPlanningParams.set("trimExtraHoursBy", planningParams.trimExtraHoursBy);
    urlPlanningParams.set("teachingStartDate", jsDateToDdMmYyyy(planningParams.teachingStartDate));
    urlPlanningParams.set("noTeachingPeriods", JSON.stringify(planningParams.noTeachingPeriods));
    urlPlanningParams.set("showHe", planningParams.showHe ? "true" : "false");

    return urlPlanningParams.toString();
}
