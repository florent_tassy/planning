export default class Day {
    dayName: string;
    dayDate: Date;

    constructor(dayName: string, dayDate: Date) {
        this.dayName = dayName;
        this.dayDate = dayDate
    }
} 
